package br.com.prontodigital.testevolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ContextMenuActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_context_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String[] jogos = {"Resident Evil", "The Legend of Zelda", "Super Mario World", "Megaman X",
                "Chrono Trigger", "Horizon Zero Dawn", "Donkey Kong", "Bomberman"};

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, jogos);

        listView = findViewById(R.id.listView);
        listView.setAdapter(arrayAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                if (listView.getCheckedItemCount() == 1)
                    mode.setTitle(listView.getCheckedItemCount() + " item selecionado");
                else
                    mode.setTitle(listView.getCheckedItemCount() + " itens selecionados");


            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context, menu);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.homeAsUp:

                    case R.id.menu_excluir:
                        excluirItens();
                        Toast.makeText(ContextMenuActivity.this, "Excluir", Toast.LENGTH_SHORT).show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    case R.id.menu_compartilhar:
                        Toast.makeText(ContextMenuActivity.this, "Compartilhar", Toast.LENGTH_SHORT).show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    case R.id.menu_info:
                        Toast.makeText(ContextMenuActivity.this, "Informação", Toast.LENGTH_SHORT).show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                listView.clearChoices();
            }
        });
    }

    public void excluirItens() {
        for (int i = 0; i < listView.getCount(); i++) {
            if (listView.isItemChecked(i)) {
//                Log.e("Item", arrayAdapter.getItem(i));
                String s = arrayAdapter.getItem(i);
                arrayAdapter.remove(s);
                arrayAdapter.notifyDataSetChanged();
            }
        }
    }
}
