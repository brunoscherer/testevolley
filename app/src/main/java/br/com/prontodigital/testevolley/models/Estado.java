package br.com.prontodigital.testevolley.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Estado {

    private int id;
    private String nome;
    private String sigla;

    public Estado() {    }

    public Estado(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getInt("geonameId");
        this.nome = jsonObject.getString("name");

        JSONObject jsonUF = jsonObject.getJSONObject("adminCodes1");
        this.sigla = jsonUF.getString("ISO3166_2");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public String toString() {
        return "ID: "+ id + "\n" +
                "Nome: " + nome + "\n" +
                "Sigla: " + sigla;
    }
}
