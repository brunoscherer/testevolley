package br.com.prontodigital.testevolley;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.prontodigital.testevolley.database.MySingleton;
import br.com.prontodigital.testevolley.models.Estado;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Request request;
    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;

    private Button btTestar;
    private Button btContextMenu;
    private TextView etTestar;
    private ProgressDialog progressDialog;
    private TextInputLayout text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Teste Volley");

        etTestar = findViewById(R.id.etTestar);
        btTestar = findViewById(R.id.btTestar);
        btContextMenu = findViewById(R.id.btContextMenu);
        text = findViewById(R.id.textTeste);

        btTestar.setOnClickListener(this);
        btContextMenu.setOnClickListener(this);

        text.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 9) {
                    text.setError("Limite máximo excedido");
                } else
                    text.setError(null);
            }
        });

        setupRequest(); // Configura request
    }

    private void setupRequest() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Testando Volley...");
        progressDialog.setCancelable(false);

        responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progressDialog.dismiss();

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("geonames");
                    String text = "";
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Estado estado = new Estado((JSONObject) jsonArray.get(i));
                        text += estado + "\n\n";
                    }
                    etTestar.setText(text);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                etTestar.setText("Erro ao conectar o server");
            }
        };
    }

    private JSONObject getLog() throws JSONException {
        JSONObject params = new JSONObject();
        params.put("idUsuario", "542252");
        params.put("idAgencia", "25");
        params.put("idAgencia", "25");
        params.put("cpf", "008220332");
        params.put("procedure", "usp_c_AppPromotorFeedNotice");
        params.put("versao", "318");
        params.put("matricula", "8810");
        params.put("idNotify", "APA91bGEqMucZioC_oLtLZSAlvys3_TQUU_cEfpUsRjrPj6rP2fQ1gJnDO24FcsFcqUV2Ago3VoXZLaxNblNQLcQf6fOEACSa2S9hqtKQaMEmpfRh1FrSMo");
        params.put("email", "contato@prontodigital.com.br");
        params.put("latitude", "0");
        params.put("longitude", "0");
        params.put("qtdVenda", "0");
        params.put("qtdPonto", "0");
        params.put("fgapp", "20");
        params.put("dadosChip", "Operadora:Oi|Slot:1|ICCID:8955316929833665751||");
        params.put("slotPadrao", "0");
        params.put("slotSMS", "0");
        params.put("memLivre", "2.00 GB");
        params.put("memTotal", "11.91 GB");
        params.put("qtdSMSEnviado", "0");
        params.put("msgSMSEnviado", "");
        params.put("qtdSMSEntregue", "0");
        params.put("msgSMSEntregue", "");
        params.put("altura", "1280");
        params.put("comprimento", "720");
        params.put("msgSMSEnviado", "");
        params.put("iccid", "8955316929833665751");
        params.put("imei", "358135090163512");
        params.put("modelo", "SM-J400M");
        params.put("api", "26");
        params.put("iccid_Sec", "");
        params.put("feedteste", "teste");

        JSONObject log = new JSONObject();
        log.put("log", params);

        return log;
    }

    private void entrar() throws JSONException {
        request = new StringRequest(Request.Method.GET,
                "http://www.geonames.org/childrenJSON?geonameId=3469034",
                responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                try {
//                    params.put("concentrador", getLog().toString());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                Log.e("PARAMS", params.toString());
                return params;
            }
        };

        Log.e("JSON", getLog().toString());
        MySingleton.getInstance(this).addToRequestQueue(request);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btTestar) {
            progressDialog.show();
            try {
                entrar();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            startActivity(new Intent(this, ContextMenuActivity.class));
        }
    }
}